package com.garranto;

import com.google.gson.*;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Scanner;

public class CurrencyConversionApp {

    public static void main(String[] args) {
        System.out.println("Currency Conversion");
        System.out.println("====================");

        //        sending the request
        OkHttpClient httpClient = new OkHttpClient().newBuilder().build();

//        parsing the response
        Gson gson = new Gson();

//        scanner object for the reading inputs from the standard input

        Scanner scanner = new Scanner(System.in);

        String apiKey = "GfRrzo6glVur4K5kJAsQxGWhmAKi1U4x";
        String amount;
        String from;
        String to;
        System.out.println("Enter the amount to convert:");
        amount = scanner.next();
        System.out.println("Enter the source currency:");
        from = scanner.next();
        System.out.println("Enter the resulting currency:");
        to = scanner.next();


        HttpUrl.Builder builder = HttpUrl.parse("https://api.apilayer.com/exchangerates_data/convert").newBuilder();

        builder.addQueryParameter("to",to);
        builder.addQueryParameter("from",from);
        builder.addQueryParameter("amount",amount);

        HttpUrl url = builder.build();

//        String url = "https://api.apilayer.com/exchangerates_data/convert?to="+to+"&from="+from+"&amount="+amount;

        Request request = new Request.Builder()
                .url(url)
                .addHeader("apiKey",apiKey)
                .method("GET", null)
                .build();

        try {
            Response response = httpClient.newCall(request).execute();

            String jsonString = response.body().string();

          JsonObject resultObject = gson.fromJson(jsonString, JsonObject.class);

          JsonPrimitive result = resultObject.getAsJsonPrimitive("result");
            System.out.println("Converted amount: "+result);


        } catch (IOException e) {
            System.out.println(e);
            System.out.println("server request failed");
        }

//        create httpclient
//        create request
//        send request
//        get response
//        process response
    }
}
//store the api key
//get the required values from the user in runtime(use scanner)
//build the request with the url, paramteres, headers(apikey)
//get the response
//access the response body as string
//convert it into corresponding object and extract the required result from the converted object

