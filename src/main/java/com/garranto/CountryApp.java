package com.garranto;

import com.google.gson.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class CountryApp {

    public static void main(String[] args) {
        System.out.println("Country App");
        System.out.println("====================");

//        sending the request
        OkHttpClient httpClient = new OkHttpClient().newBuilder().build();

//        parsing the response

        Gson gson = new Gson();



        Request request = new Request.Builder()
                .url("https://restcountries.com/v3.1/all")
//                .addHeader("","")
                .method("GET",null)
                .build();

        try {
            Response response = httpClient.newCall(request).execute();
//            System.out.println(response.body().string());

            String jsonString = response.body().string();

            JsonArray countries = gson.fromJson(jsonString,JsonArray.class);

            System.out.println("Total Countries:"+countries.size());
            System.out.println("====================");

            for(JsonElement element:countries){

                JsonObject country = gson.fromJson(element,JsonObject.class);
                JsonObject nameObject = country.getAsJsonObject("name");
                JsonPrimitive countryOfficialName = nameObject.getAsJsonPrimitive("official");
                System.out.println(countryOfficialName);
            }



        } catch (IOException e) {
            System.out.println("server request failed");
        }

//        create httpclient
//        create request
//        send request
//        get response
//        process response
    }
}

//gson
//jsonstring to jsonobject
//jsonobject to string


//jsonelement
//jsonarray
//jsaonobject
//jsonprimitive

//array
//object
//primitives


//amount
//to
//from
//apikey